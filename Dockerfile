#
# Dockerfile for badsectorlabs/code-checking
#
# Maintainer:
# Erik <erik@badsectorlabs.com>
# Based on: https://gitlab.cern.ch/ComputerSecurity/Security-Services-Code-Checking

#
# Base image and image metadata
#
FROM centos:latest
MAINTAINER "Erik <erik@badsectorlabs.com>"

#
# Install with YUM:
# - Epel Repo
# - Basic development tools (3)
# - Java development packages
# - Python development packages (2)
# - Python PIP
# - General useful software (3)
# - PyChecker
# - PyLint
# - Flake8
#
RUN yum -y install epel-release && \
    yum -y update && \
    yum -y groups mark convert && \
    yum -y groupinstall 'Development Tools' && \
    yum -y install java-1.8.0-openjdk java-1.8.0-openjdk-devel && \
    yum -y install python python-devel python3 && \
    yum -y install python-flake8 python-pep8 && \
    yum -y install wget unzip unrar sudo && \
    yum -y install p7zip p7zip-plugins && \
    yum -y install htop vim && \
    yum -y install expat expat-devel && \
    yum -y install flex flex-devel && \
    yum -y install pychecker && \
    yum -y clean all

RUN yum -y install https://centos7.iuscommunity.org/ius-release.rpm && \
    yum -y install python36u && \
    yum -y install python36u-pip && \
    pip3.6 install pylint && \
    pip3.6 install flake8


WORKDIR /opt

#
# Install PMD (and CPD)
#
RUN wget https://github.com/pmd/pmd/releases/download/pmd_releases%2F5.5.1/pmd-bin-5.5.1.zip && \
    unzip pmd-bin-5.5.1.zip && \
    rm -f pmd-bin-5.5.1.zip && \
    echo '/opt/pmd-bin-5.5.1/bin/run.sh cpd "$@"' >> /usr/local/bin/cpd && \
    echo '/opt/pmd-bin-5.5.1/bin/run.sh pmd "$@"' >> /usr/local/bin/pmd && \
    chmod +x /usr/local/bin/cpd && \
    chmod +x /usr/local/bin/pmd

#
# Finish
#
WORKDIR /
