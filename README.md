# badsectorlabs/code-checking

This repository contains the Dockerfile used
to build the **badsectorlabs/code-checking** Docker image for automated
code checking with Gitlab CI.

Based on: https://gitlab.cern.ch/ComputerSecurity/Security-Services-Code-Checking


## Using badsectorlabs/code-checking image on local machine

Just execute the following command:
```
docker run -i -t --entrypoint /bin/bash badsectorlabs/code-checking:latest
```

---


## Building on local computer

```
$ docker build -t code-checking:latest .

Please, note that Docker service must be running on host.
```

Then start a container from newly created local image:
```
docker run -i -t --entrypoint /bin/bash badsectorlabs/code-checking:latest
```

---

---
## Use in .gitlab-ci.yml (Python 3)

```yaml
test:
  stage: test
  image: badsectorlabs/code-checking:latest
  script:
    - cpd --minimum-tokens 100 --language python --files .
    # pylint output is good to look at, but not worth breaking the build over
    - pylint -d bad-continuation -d line-too-long -d import-error -d missing-docstring . || true
    - flake8 --max-line-length 120 --ignore=E722,W503 . # You must pass flake8 (W503 is wrong, pep8 changed)
```


---


## References

\[1\]
[Automatic Docker's images building -- CERN's Gitlab-CI examples]
(https://gitlab.cern.ch/gitlabci-examples/build_docker_image)

\[2\]
[Dockerfile reference]
(https://docs.docker.com/engine/reference/builder/)

\[3\]
[Static Code Analysis with Gitlab-CI -- CERN's Gitlab-CI examples]
(https://gitlab.cern.ch/gitlabci-examples/static_code_analysis)

\[4\]
[Static Code Analysis with Gitlab-CI -- Summer Student report 2016]
(https://security.web.cern.ch/security/reports/reports/2016/Datko.pdf)

\[5\]
[Automate your life with Gitlab-CI -- Students Session 2016 (video)]
(https://cds.cern.ch/record/2206413)
